<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/info',function(Request $request){
    return $request->header('origin');

    $output = shell_exec('wp --info');

    return $output;
});



// function CreateDB($request){

//     $output = shell_exec("sudo mysql   -e \"create database {$request->db_name}; CREATE USER '{$request->db_username}'@'%' IDENTIFIED BY '{$request->db_password}';\"");

//     $output = shell_exec(`sudo mysql   -e "GRANT ALL PRIVILEGES ON {$request->db_name}.* TO '{$request->db_username}'@'%';"`);

// }

// function DropDB($request){

//     $output = shell_exec("sudo mysql   -e \"drop database {$request->db_name}; \"");

// }

Route::post('/download-wordpress',function(Request $request){
    
 
    // download wordpress
    shell_exec(" sudo -u {$request->system_user} -i -- wp core download --path=/home/{$request->system_user}/{$request->reserve_domain}/public");


    return 'OK';
});


Route::post('/config-wordpress',function(Request $request){
    
 
        // create config
        shell_exec("sudo -u {$request->system_user} -i -- wp config create --dbname={$request->db_name} --dbuser={$request->db_username} --dbpass={$request->db_password} --locale=id_ID --path=/home/{$request->system_user}/{$request->reserve_domain}/public");

   
    
    return 'OK';
});
Route::post('/install-wordpress',function(Request $request){
    
 
       // install wordpress
       shell_exec("sudo -u {$request->system_user} -i -- wp core install --url={$request->reserve_domain} --title='{$request->website_title}' --admin_user={$request->website_username} --admin_password={$request->website_password} --admin_email={$request->website_email} --path=/home/{$request->system_user}/{$request->reserve_domain}/public");
    

return 'OK';
});


Route::post('/change-wp-domain',function(Request $request){
    
 
    // install wordpress
    shell_exec("sudo -u {$request->system_user} -i -- wp search-replace '{$request->old_domain}' '{$request->main_domain}' --path=/home/{$request->system_user}/{$request->reserve_domain}/public");
 

return 'OK';
});

Route::post('/force-https',function(Request $request){
    
 
    // install wordpress
    shell_exec("sudo -u {$request->system_user} -i -- wp search-replace 'http' 'https' --path=/home/{$request->system_user}/{$request->reserve_domain}/public");
  
    return 'OK';
});


Route::post('/back-to-http',function(Request $request){
    
 
    // install wordpress
    shell_exec("sudo -u {$request->system_user} -i -- wp search-replace 'https' 'http' --path=/home/{$request->system_user}/{$request->reserve_domain}/public");
  
    return 'OK';
});

 


Route::post('/create-database',function(Request $request){
    
    $output = shell_exec("sudo    mysql   -e \"create database {$request->db_name}; CREATE USER '{$request->db_username}'@'localhost' IDENTIFIED BY '{$request->db_password}';\"");

    $output = shell_exec("sudo    mysql   -e \"GRANT ALL PRIVILEGES ON {$request->db_name}.* TO '{$request->db_username}'@'localhost';\"");

 
    return 'OK';
});



Route::post('/drop-database',function(Request $request){
     
    $output = shell_exec("sudo mysql   -e \"drop database {$request->db_name}; \""); 
 
    return 'OK';
});



Route::post('/change-system-user-password',function(Request $request){
    
   

    $command = sprintf(
        "echo '%s:%s' | sudo chpasswd", 
        escapeshellarg($request->system_user), 
        escapeshellarg($request->system_user_pass)
    );

    exec($command, $output, $return);

     shell_exec("sudo passwd -x 1 {$request->system_user}");

     if ($return === 0) {
        // success.
        return 'OK';
     }
});

Route::post('/dump-db',function(Request $request){
    
    $output = shell_exec(" sudo -u {$request->system_user} -i -- wp core download --path=/home/{$request->system_user}/{$request->reserve_domain}/public");

    return $output;
});



Route::post('/install-cloudflare-plugin',function(Request $request){
    
    $output = shell_exec(" sudo -u {$request->system_user} -i -- wp plugin install cloudflare-flexible-ssl --activate    --path=/home/{$request->system_user}/{$request->reserve_domain}/public");

    return $output;
});

Route::post('/get-wp-config',function(Request $request){
    
    $output = shell_exec(" sudo -u {$request->system_user} -i -- wp config list DB_USER DB_PASSWORD DB_NAME --json   --path=/home/{$request->system_user}/{$request->reserve_domain}/public");

    return $output;
});



Route::post('/copy-certificate',function(Request $request){
    
    shell_exec("sudo  cp -rf /etc/nginx/ssl/{$request->server_url} /etc/nginx/ssl/{$request->reserve_domain}");

    shell_exec("sudo  service nginx reload");

    return 'OK';
});